import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  
  todoItems:any = [
    {
      text: "Sacar la basura",
      isComplete: false
    },
    {
      text: "Ver el repo",
      isComplete: true
    },
    {
      text: "Seguir al profe twitter",
      isComplete: false
    }
  ];
  
  constructor() { }

  ngOnInit() {
  }

  addTodoItem(){

  }

  markComplete(index){
    this.todoItems[index]["isComplete"] = !this.todoItems[index]["isComplete"];
  }

}
